# unbouncer

Unbouncer: Controlled DNS cache poisoning for your convenience.
Sources a large collection of blacklists from numerous sources and outputs a unbound ready configfile.
Blackhole annoying ads, tracking and phishing domains.

Works with Unbound DNS validating resolver, example unbound config file is included in this repository.

OS requirements:
- unbound (an unbound example config file is included in this repo)
- parallel (gnu parallel)
- dos2unix
- wget

An ansible deployment is provided in the root of this repo for easy install

Run daily in cron to keep your dns resolver and ns protection up to date.

[PayPal.Me](https://paypal.me/jnick85?locale.x=nl_NL)
