#!/bin/bash

# 14-04-2019, Jannick Sikkema, initial build.
# Unbouncer: Controlled DNS cache poisoning for your convenience
# Blacklist sources (main): https://firebog.net & github

# Global variables
IFS=$'\n'
LC_ALL='C'
APPNAME='unbouncer'
SELF_CONFIG_DIR=$(cd ${0%/*} && pwd)
SELF_CONFIG_FILE="${SELF_CONFIG_DIR}/${APPNAME}.conf"
LOCKFILE="/var/lock/subsys/${APPNAME}"
WEIGHT=0
declare -i WEIGHT
declare -a black_lists manual_black_list

# Checks for existing configuration files, fallback on defaults when not found.
load_config () {
    if [[ -f ${SELF_CONFIG_FILE} ]]; then
        source ${SELF_CONFIG_FILE}
    else
        if [[ -f /opt/${APPNAME}/${APPNAME}.conf ]]; then
            SELF_CONFIG_DIR="/opt/${APPNAME}"
            source ${SELF_CONFIG_DIR}/${APPNAME}.conf
        else
            print_debug "Could not find config file: ${SELF_CONFIG_FILE}, loading built-in variables."
            SELF_CONFIG_DIR='/tmp'
            IP=''
            CONF_DEST='/etc/unbound/blocked_domains.conf'
            DEBUGMODE='false'
            DEPLOY_CONF='false'
            APPEND_MODE='true'
            NO_SIGNATURE_CHECK='false'
        fi
    fi
    HTTP_BLACK_LISTS="${SELF_CONFIG_DIR}/black_lists.cfg"
    if [[ ! -f ${HTTP_BLACK_LISTS} ]]; then
        print_debug "Could not find ${HTTP_BLACK_LISTS}, loading defaults from built-in array."
        #Load a list of of reputable black lists to an array.
        black_lists=(https://raw.githubusercontent.com/StevenBlack/hosts/master/data/add.Risk/hosts
        https://raw.githubusercontent.com/StevenBlack/hosts/master/data/add.2o7Net/hosts
        https://raw.githubusercontent.com/StevenBlack/hosts/master/data/add.Spam/hosts
        https://raw.githubusercontent.com/StevenBlack/hosts/master/data/KADhosts/hosts
        https://raw.githubusercontent.com/StevenBlack/hosts/master/data/UncheckyAds/hosts
        https://raw.githubusercontent.com/StevenBlack/hosts/master/hosts
        http://winhelp2002.mvps.org/hosts.txt
        https://zeustracker.abuse.ch/blocklist.php?download=domainblocklist
        https://zeustracker.abuse.ch/blocklist.php?download=hostfile
        http://sysctl.org/cameleon/hosts
        https://www.dshield.org/feeds/suspiciousdomains_High.txt
        https://www.dshield.org/feeds/suspiciousdomains_Medium.txt
        https://v.firebog.net/hosts/AdguardDNS.txt
        https://v.firebog.net/hosts/Easylist.txt
        https://v.firebog.net/hosts/Easyprivacy.txt
        https://v.firebog.net/hosts/Prigent-Ads.txt
        https://v.firebog.net/hosts/Prigent-Malware.txt
        https://v.firebog.net/hosts/Prigent-Phishing.txt
        https://someonewhocares.org/hosts/zero/hosts
        https://raw.githubusercontent.com/AdguardTeam/AdGuardSDNSFilter/master/Filters/rules.txt
        https://pgl.yoyo.org/adservers/admiral-domains.txt
        https://pgl.yoyo.org/as/serverlist.php
        https://adaway.org/hosts.txt)
    fi
    WHITE_LIST="${SELF_CONFIG_DIR}/white_list.cfg"
    WHITE_LIST_FIXED="${SELF_CONFIG_DIR}/white_list_fixed.cfg"
    MANUAL_BLACK_LIST="${SELF_CONFIG_DIR}/manual_black_list.cfg"
    TMPDIR="${SELF_CONFIG_DIR}/temp"
    SIGNATURE_LIST="${SELF_CONFIG_DIR}/signatures.txt"
    TMPFILE="${TMPDIR}/block_hosts.tmp"
    BLOCKSFILE="${TMPDIR}/unbound_blocks.tmp"
    UNBOUNDFILE="${TMPDIR}/blocked_domains.tmp"
}

pause () {
    if [[ ${PAUSE_TIME} ]]; then
        sleep ${0}
    else
        sleep 2
    fi
}

# Checks arguments for existing binaries (like unbound), exit on fail.
check_binary () {
    for bin in ${@}; do
        command -v ${bin} &>/dev/null
        cmd_result="$?"
        if [[ ${cmd_result} -ne '0' ]]; then
            echo "ERROR: Cannot find ${bin}, requirement: ${bin} not found exiting..."
            logger -ip user.error "ERROR: Cannot find ${bin}, requirement: ${bin} not found."
            exit 1
        fi
    done
}

interrupt_handler () {
    trap 'cleanup_handler' 1 2 3 15
}

cleanup_handler () {
    rm -f ${TMPDIR}/*.tmp 2> /dev/null
    unlink "$TMPFILE"¬ 2> /dev/null
    unlink "$BLOCKSFILE"¬ 2> /dev/null
    unlink "$CONF_DEST"_validate.tmp¬ 2> /dev/null
    unlink "$CONF_DEST".tmp¬ 2> /dev/null
    unlink "$LOCKFILE" 2> /dev/null
    exit
}

# Checks for existence of the lockfile, creates it when it is not in place.
check_lockfile () {
    if [[ ! -f ${LOCKFILE} ]]; then
        touch "$LOCKFILE"
    else
        print_info "ERROR: Lockfile in place: ${LOCKFILE}, ${APPNAME} appears to be running. exiting..."
        exit 1
    fi
}

# Simple function to skip hashes and blank lines in for loops.
skip_hashes () {
    unset loop_operator
    if [[ `printf ${@} | grep -E "^#|^$"` ]]; then
        loop_operator="continue"
        if [[ ${DEBUGMODE} == 'true' ]]; then
            if [[ -n ${@} ]]; then
                print_debug "-- ${@} -- Ignored."
            fi
        fi
    fi
}

# Compare black list signatures, lists with unmodified signatures will be skipped
signature_check () {
    if [[ ${NO_SIGNATURE_CHECK} = 'true' ]]; then
        return
    fi
    unset loop_operator
    touch ${SIGNATURE_LIST}
    if [[ `cat ${SIGNATURE_LIST} | wc -l` -gt '5000' ]]; then
        print_debug "Flushing signatures in ${SIGNATURE_LIST}, signature capacity reached."
        echo '' > ${SIGNATURE_LIST}
    fi
    if [[ `grep -F "$signature" "$SIGNATURE_LIST"` ]]; then
        print_info "Black list contents unchanged, skipping..."
        loop_operator="continue"
        if [[ ${DEBUGMODE} == 'true' ]]; then
            print_debug "${signature} skipped due signature match, BL contents not modified since last fetch."
        fi
    else
        echo ${signature} >> ${SIGNATURE_LIST}
    fi
    unset signature
}

# Fetch lists, read from black lists array, fetch them from the net and build them.
fetch_lists () {
    declare -a black_lists=`cat ${HTTP_BLACK_LISTS}`
    print_info "Fetching black lists..."
    mkdir -p ${TMPDIR}
    cd ${TMPDIR}
    for list in ${black_lists[@]}; do
        skip_hashes ${list}
        ${loop_operator}
        if [[ ! -f ${list} ]]; then
            print_info "Retrieving: `basename ${list}` from ${list}"
            file='inet_hosts_blacklist.tmp'
            if [[ "$DEBUGMODE" == 'true' ]]; then
                wget -t 3 --timeout=3 ${list} --output-document=${file} -nv \
                --user-agent=chrome -e server_response=on -e timestamping=off
            else
                wget -t 3 --timeout=3 ${list} --output-document=${file} -q \
                --user-agent=chrome -e server_response=off -e timestamping=off
            fi
            wget_result="$?"
            if [[ -f ${SELF_CONFIG_DIR}/keepfiles ]]; then
                cp ${file} "${TMPDIR}/bl_$(echo $list | tr "/" "_" | tr -d ":")_$(date +%s).txt"
            fi
            signature="bl_`echo $list | tr "/" "_" | tr -d ":"`/$(md5sum $file | awk '{print $1}')"
            if [[ ${wget_result} -ne '0' ]]; then
                rm -f ${file}
                print_info "Error fetching: `basename ${list}` from ${list}, wget error code was: ${wget_result}."
                unset wget_result file
                printf $'\n'
                continue
            fi
        else
            file=${list}
        fi
        signature_check
        ${loop_operator}
        print_debug "fetching data from: $list parsing: ${file}"
        parse_file ${file}
        rm -f ${file}
        unset file
        printf $'\n'
    done
}

# print to term or logger depending on presence of tty
print_info () {
    if [[ -t 1 ]]; then
        echo "INFO: $@"
    else
        logger -ip user.info "${0}: INFO: ${@}"
    fi
}

# debug mode function, enable with boolean.
print_debug () {
    if [[ ${DEBUGMODE} == 'true' ]]; then
        echo "DEBUG: $1"
        if [[ -f ${2} ]]; then
            echo "Showing shuffled data from $2:"
            shuf -n 5 "$2"
            echo "----------"
            echo "Showing last 5 entries from $2:"
            tail -n 5 "$2"
        fi
        printf $'\n'
    fi
}

# Parse file 'manual' from the manually created list.
parse_file_manually () {
    declare -a manual_black_list=`cat ${MANUAL_BLACK_LIST}`
    print_info "Implementing manual blacklist..."
    for ln in ${manual_black_list[@]}; do
        skip_hashes ${ln}
        ${loop_operator}
        echo "block: ${ln}" | awk '{print $1,$2}'
    done >> "$TMPFILE"
    print_debug "show TMPFILE:" "$TMPFILE"
}

# Parse file list, finding lines with fully qualified domain names (fqdns).
parse_file_list () {
    for ln in `echo "sort -u ${1} | grep -v '#' | grep -Eo '([a-z0-9][a-z0-9\-]{0,61}[a-z0-9]\.)+[a-z0-9][a-z0-9\-]*[a-z0-9]'" | parallel`; do
        echo "127.0.0.1 $ln"
    done >> "$TMPFILE"
    print_debug `ls -s "$TMPFILE" --block-size=K`
    print_debug "show TMPFILE:" "$TMPFILE"
}

# Parse file, finding lines with blackhole IP's, like 127.0.0.1 and 0.0.0.0
parse_file () {
    if [[ -z ${parse_nr} ]]; then
        parse_nr='1'
    else
        parse_nr=`expr ${parse_nr} + 1`
    fi
    print_info "${parse_nr}-> Parsing, ${1} ..."
    if [[ `grep -E "127\.0\.0\.1|0\.0\.0\.0" ${1} | wc -l` -ge 10 ]]; then
        for ln in `echo "sort -u ${1} | sed 's/#.*//' | grep -E '^127\.0\.0\.1|^0\.0\.0\.0'" | parallel`; do
            echo "$ln"
        done >> "$TMPFILE"
        print_debug `ls -s "$TMPFILE" --block-size=K`
        print_debug "show TMPFILE:" "$TMPFILE"
    else
        parse_file_list ${1}
    fi
}

# Create temporary blacklist based on hosts files with unified formatting
create_tmp_file () {
    print_info "Creating block file..."
    sort -i "$TMPFILE" |
        dos2unix |
        awk '{print $2}' |
        tr -t [:upper:] [:lower:] |
        tr -d "[:blank:]" |
        tr -s '.' |
        grep -E "[[:alpha:]]" |
        grep -Ev "\/|\ \-|\&|^\.|\.$|^co\.uk$|^com\.au$|^co\.za$" |
    awk '{print NR, $0}' >> $TMPDIR/domain_list.tmp
    print_debug "Showing temporary domain list." "$TMPDIR/domain_list.tmp"
    LINECNT=`wc -l $TMPDIR/domain_list.tmp | awk '{ print $1}'`
    print_debug "building segment lists."
    print_info "Found "$LINECNT" domains to blacklist..."
    if [[ -n "$IP" ]]; then
        local_data_build
    else
        local_zone_build
    fi
    print_debug "showing BLOCKSFILE, $BLOCKSFILE:" "$BLOCKSFILE"
    rm -f $TMPDIR/*_list.tmp
    unlink "$TMPFILE"
    print_info "Unduping and sorting block list..."
    sort "$BLOCKSFILE" | uniq >> "$UNBOUNDFILE"
}

# Make unbound answer queries in blocklist with an ip address, enabled when $IP has a value
local_data_build () {
    for i in `seq "$LINECNT"`; do
        echo "$i local-zone: \"" >> "$TMPDIR/zone_list.tmp"
        echo "$i \" redirect local-data: \"" >> "$TMPDIR/redirect_list.tmp"
        echo "$i A $IP\"" >> "$TMPDIR/ip_list.tmp"
    done
    print_debug "joining segment lists."
    join $TMPDIR/zone_list.tmp $TMPDIR/domain_list.tmp >> $TMPDIR/front_list.tmp
    join $TMPDIR/redirect_list.tmp $TMPDIR/domain_list.tmp >> $TMPDIR/centre_list.tmp
    join $TMPDIR/centre_list.tmp $TMPDIR/ip_list.tmp >> $TMPDIR/back_list.tmp
    join $TMPDIR/front_list.tmp $TMPDIR/back_list.tmp >> $TMPDIR/final_list.tmp
    awk '{$1=""; print substr($0,2)}' $TMPDIR/final_list.tmp | sed -e 's/" /"/g' |
        sed -e 's/ "redirect /" redirect /g' >> "$BLOCKSFILE"
}

# Make unbound answer queries in blocklist as NXDOMAIN, enabled when $IP = empty
local_zone_build () {
    for i in `seq "$LINECNT"`; do
        echo "$i local-zone: \"" >> "$TMPDIR/zone_list.tmp"
        echo "$i \"_static" >> "$TMPDIR/static_list.tmp"
    done
    print_debug "joining segment lists."
    join $TMPDIR/zone_list.tmp $TMPDIR/domain_list.tmp >> $TMPDIR/front_list.tmp
    join $TMPDIR/front_list.tmp $TMPDIR/static_list.tmp >> $TMPDIR/final_list.tmp
    awk '{$1=""; print substr($0,2)}' $TMPDIR/final_list.tmp |
        sed -e 's/" /"/g' | sed -e 's/ "_static/" static/g' >> "$BLOCKSFILE"
}

# Implement whitelist
enable_whitelist () {
    print_info "Implementing whitelist..."
    print_debug "Showing ${UNBOUNDFILE}:" ${UNBOUNDFILE}
    grep -Ev $(sort ${WHITE_LIST} | grep -Ev "^$|^#"| tr -s '\n' '|' | sed '$ s/|$//') \
        ${UNBOUNDFILE} >> ${UNBOUNDFILE}_wl_regex
    print_debug 'Showing regex capable whitelist:' ${UNBOUNDFILE}_wl_regex
    grep -Ev $(sort ${WHITE_LIST_FIXED} | grep -Ev "^$|^#"| tr -s '\n' '|' | sed -r 's/(\s?\.)/\\./g' | sed '$ s/|$//') \
        ${UNBOUNDFILE}_wl_regex >> ${UNBOUNDFILE}_wl_fixed
    print_debug "Showing fixed whitelist:" ${UNBOUNDFILE}_wl_fixed
    bl_cnt=`wc -l ${UNBOUNDFILE} | awk '{ print $1}'`
    wl_cnt=`wc -l ${UNBOUNDFILE}_wl_fixed | awk '{ print $1}'`
    print_info "Found ${bl_cnt} unique domains to blacklist..."
    if [[ ${wl_cnt} -lt ${bl_cnt} ]]; then
        subtract=$(expr ${wl_cnt} - ${bl_cnt})
        print_info "Whitelisting subtracted ${subtract} domains..."
    fi
    unlink ${UNBOUNDFILE}_wl_regex
    mv ${UNBOUNDFILE}_wl_fixed ${UNBOUNDFILE}
}

# Check for presence of existing config and append to new block list
append_existing_config () {
    print_info "Building block list with a weight of: ${WEIGHT}"
    sort ${TMPFILE} | uniq -c | awk -v WEIGHT="$WEIGHT" '$1>WEIGHT {print $2,$3}' | sed -e "s/ *$//" >> ${TMPFILE}_wrk
    mv ${TMPFILE}_wrk ${TMPFILE}
    if [[ "$APPEND_MODE" = 'true' ]] && [[ -f "$CONF_DEST" ]]; then
        print_info "Appending existing config: ${CONF_DEST}"
        awk '{print $1,$2}' ${CONF_DEST} | grep -Ev "^$|^#" | tr -d '"' >> "$TMPFILE"
    else
        if [[ ! -f "$CONF_DEST" ]]; then
            print_debug "Skipped appending existing blocks due no existing config found: ($CONF_DEST)."
        else
            print_debug "Skipped appending existing blocks due APPEND_MODE = false"
        fi
        return 0
    fi
}

# Remove duplicate entries from configuration
trim_duplicate_entries () {
    print_debug "Removing duplicate entries from domain list"
    mv ${TMPFILE} ${TMPFILE}.tmp
    sort -i ${TMPFILE}.tmp | uniq >> ${TMPFILE}
    unlink ${TMPFILE}.tmp
    print_debug "showing sorted TMPFILE:" ${TMPFILE}
}

# Check unbound blockfile for errors and deploy configuration
deploy_conf () {
    print_info "Sanity checking unbound configuration and deploying config..."
    sed -i "1i #Config build by ${APPNAME}" "$UNBOUNDFILE"
    sed -i "2i server:" "$UNBOUNDFILE"
    touch "$CONF_DEST"
    touch "$CONF_DEST"_validate.tmp
    mv "$UNBOUNDFILE" "$CONF_DEST".tmp
    if [[ -n "$IP" ]]; then
        head -n 25000 "$CONF_DEST".tmp >> "$CONF_DEST"_validate.tmp
        unbound-checkconf "$CONF_DEST"_validate.tmp
    else
        unbound-checkconf "$CONF_DEST".tmp
    fi
    if [[ ${?} == '0' ]]; then
        unlink "$CONF_DEST"_validate.tmp
        if [[ "$DEPLOY_CONF" == 'true' ]]; then
            mv -vf "$CONF_DEST" "$CONF_DEST".bak
            mv -vf "$CONF_DEST".tmp "$CONF_DEST"
            print_info "New blacklisted domains count: $(expr `cat ${CONF_DEST} | wc -l` - `cat ${CONF_DEST}.bak | wc -l`)"
            unlink "$BLOCKSFILE"
            unbound-control reload
            print_info "All done!"
            unlink "$LOCKFILE"
            exit 0
        fi
        unlink "$BLOCKSFILE"
        mv "$CONF_DEST".tmp "$SELF_CONFIG_DIR/${APPNAME}_domains_conf.txt"
        print_info "All done!"
        unlink "$LOCKFILE"
        exit 0
    else
        unlink "$BLOCKSFILE"
        mv "${CONF_DEST}.tmp" "${CONF_DEST}_${APPNAME}.save"
        unlink "$CONF_DEST"_validate.tmp
        print_info "Error in unbound check conf, please check your unbound configuration, exiting..."
        logger -ip user.error "ERROR encountered validating new unbound config, saved as: ${CONF_DEST}_${APPNAME}.save"
        unlink "$LOCKFILE"
        exit 1
    fi
}

#
# Main function
#
main_exec () {
    check_binary unbound parallel dos2unix wget
    check_lockfile
    interrupt_handler
    load_config
    fetch_lists
    append_existing_config
    parse_file_manually
    trim_duplicate_entries
    create_tmp_file
    enable_whitelist
    deploy_conf
}

main_exec
