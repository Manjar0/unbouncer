unbouncer
=========

Ansible role to quickly configure a single network interface, unbound service and the unbouncer dns ad blocker with a base config

Network variables can be edited in the main tasks file. (TODO: make these actually variable)

Requirements
------------

Linux compatible only

Role Variables
--------------

-

Dependencies
------------

None

Example Playbook
----------------

    - hosts: all
      roles:
         - { role: unbouncer }

License
-------

BSD

Author Information
------------------

J. Sikkema
